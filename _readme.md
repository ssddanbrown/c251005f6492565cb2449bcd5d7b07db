This is a hack to add a simple latest-page RSS feed to the BookStack using the logical theme system.

### Setup

This uses the [logical theme system](https://github.com/BookStackApp/BookStack/blob/development/dev/docs/logical-theme-system.md).

1. Within the BookStack install folder, you should have a `themes` folder.
2. Create a `themes/custom/functions.php` file with the contents of the `functions.php` file example below.
3. Add `APP_THEME=custom` to your .env file.
4. Save the contents of the `rss.blade.php` file below to a `themes/custom/rss.blade.php` file.

### Note

These customizations are not officially supported any may break upon, or conflict with, future updates. 
Quickly tested on BookStack v22.11.1.